<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bed".
 *
 * @property int $bed_id
 * @property int|null $dep_id
 * @property string|null $bed_name
 *
 * @property Appoint[] $appoints
 * @property Department $dep
 */
class Bed extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bed';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dep_id'], 'integer'],
            [['bed_name'], 'string', 'max' => 30],
            [['dep_id'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['dep_id' => 'dep_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'bed_id' => 'Bed ID',
            'dep_id' => 'Dep ID',
            'bed_name' => 'เตียง',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppoints()
    {
        return $this->hasMany(Appoint::className(), ['bed_id' => 'bed_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDep()
    {
        return $this->hasOne(Department::className(), ['dep_id' => 'dep_id']);
    }
}
