<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ttm".
 *
 * @property string|null $appoint_date
 * @property int|null $bed_id
 * @property int|null $p1
 * @property int|null $p2
 * @property int|null $p3
 * @property int|null $p4
 * @property int|null $p5
 */
class Ttm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttm';
    }

    public static function primaryKey()
    {
        return ['bed_id'];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['appoint_date','bed_name'], 'safe'],
            [['bed_id', 'p1', 'p2', 'p3', 'p4', 'p5','p6'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'appoint_date' => 'วันนัด',
            'bed_id' => 'รหัสเตียง',
            'bed_name' => 'เตียง',
            'p1' => '9.00-10.00 น.',
            'p2' => '10.30-11.30 น.',
            'p3' => '13.30-14.30 น.',
            'p4' => '14.30-15.30 น.',
            'p5' => '16.30-17.30 น.',
            'p6' => '18.00-19.00 น.',
        ];
    }
}
