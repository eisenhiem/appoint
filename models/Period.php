<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "period".
 *
 * @property int $period_id
 * @property string|null $period_name
 *
 * @property Appoint[] $appoints
 */
class Period extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'period';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['period_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'period_id' => 'Period ID',
            'period_name' => 'ช่วงเวลา',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppoints()
    {
        return $this->hasMany(Appoint::className(), ['period_id' => 'period_id']);
    }
}
