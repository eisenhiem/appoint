<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "appoint".
 *
 * @property int $appoint_id
 * @property int|null $dep_id
 * @property int|null $bed_id
 * @property int|null $reserv_id
 * @property int|null $period_id
 * @property string|null $appoint_date
 * @property string|null $appoint_status
 * @property string|null $d_update
 *
 * @property Period $period
 * @property Bed $bed
 * @property Department $dep
 * @property Reserv $reserv
 */
class Appoint extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'appoint';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dep_id', 'bed_id', 'reserv_id', 'period_id'], 'integer'],
            [['appoint_date', 'd_update'], 'safe'],
            [['appoint_status'], 'string'],
            [['period_id'], 'exist', 'skipOnError' => true, 'targetClass' => Period::className(), 'targetAttribute' => ['period_id' => 'period_id']],
            [['bed_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bed::className(), 'targetAttribute' => ['bed_id' => 'bed_id']],
            [['dep_id'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['dep_id' => 'dep_id']],
            [['reserv_id'], 'exist', 'skipOnError' => true, 'targetClass' => Reserv::className(), 'targetAttribute' => ['reserv_id' => 'reserv_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'appoint_id' => 'Appoint ID',
            'dep_id' => 'แผนก',
            'bed_id' => 'เตียง',
            'reserv_id' => 'ผู้รับบริการ',
            'period_id' => 'เวลา',
            'appoint_date' => 'วันนัด',
            'appoint_status' => 'สถานะ',
            'd_update' => 'วันที่ปรับปรุงล่าสุด',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriod()
    {
        return $this->hasOne(Period::className(), ['period_id' => 'period_id']);
    }

    public function getPeriodName(){
        return $this->period->period_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBed()
    {
        return $this->hasOne(Bed::className(), ['bed_id' => 'bed_id']);
    }

    public function getBedName(){
        return $this->bed->bed_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDep()
    {
        return $this->hasOne(Department::className(), ['dep_id' => 'dep_id']);
    }

    public function getDepName(){
        return $this->dep->dep_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReserv()
    {
        return $this->hasOne(Reserv::className(), ['reserv_id' => 'reserv_id']);
    }

    public function getReservName(){
        return $this->reserv->reserv_name;
    }

    public function getReservTel(){
        return $this->reserv->reserv_tel;
    }

}
