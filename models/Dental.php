<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dental".
 *
 * @property string|null $appoint_date
 * @property int|null $bed_id
 * @property int|null $p1
 * @property int|null $p2
 * @property int|null $p3
 */
class Dental extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dental';
    }

    public static function primaryKey()
    {
        return ['bed_id'];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['appoint_date'], 'safe'],
            [['bed_id', 'p1', 'p2', 'p3'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'appoint_date' => 'Appoint Date',
            'bed_id' => 'Bed ID',
            'p1' => 'P1',
            'p2' => 'P2',
            'p3' => 'P3',
        ];
    }
}
