<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reserv".
 *
 * @property int $reserv_id
 * @property string|null $reserv_name
 * @property string|null $reserv_tel
 *
 * @property Appoint[] $appoints
 */
class Reserv extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reserv';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reserv_name', 'reserv_tel'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'reserv_id' => 'Reserv ID',
            'reserv_name' => 'ชื่อผู้รับบริการ',
            'reserv_tel' => 'เบอร์โทรติดต่อกลับ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppoints()
    {
        return $this->hasMany(Appoint::className(), ['reserv_id' => 'reserv_id']);
    }
}
