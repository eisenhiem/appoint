<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Appoint;

/**
 * AppointSearch represents the model behind the search form of `app\models\Appoint`.
 */
class AppointSearch extends Appoint
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['appoint_id', 'dep_id', 'bed_id', 'reserv_id', 'period_id'], 'integer'],
            [['appoint_date', 'appoint_status', 'd_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Appoint::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'appoint_id' => $this->appoint_id,
            'dep_id' => $this->dep_id,
            'bed_id' => $this->bed_id,
            'reserv_id' => $this->reserv_id,
            'period_id' => $this->period_id,
            'appoint_date' => $this->appoint_date,
            'd_update' => $this->d_update,
        ]);

        $query->andFilterWhere(['like', 'appoint_status', $this->appoint_status]);

        return $dataProvider;
    }
}
