<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Reserv;

/**
 * ReservSearch represents the model behind the search form of `app\models\Reserv`.
 */
class ReservSearch extends Reserv
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reserv_id'], 'integer'],
            [['reserv_name', 'reserv_tel'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Reserv::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'reserv_id' => $this->reserv_id,
        ]);

        $query->andFilterWhere(['like', 'reserv_name', $this->reserv_name])
            ->andFilterWhere(['like', 'reserv_tel', $this->reserv_tel]);

        return $dataProvider;
    }
}
