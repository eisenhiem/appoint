<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Dental;

/**
 * DentalSearch represents the model behind the search form of `app\models\Dental`.
 */
class DentalSearch extends Dental
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['appoint_date'], 'safe'],
            [['bed_id', 'p1', 'p2', 'p3'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dental::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'appoint_date' => $this->appoint_date,
            'bed_id' => $this->bed_id,
            'p1' => $this->p1,
            'p2' => $this->p2,
            'p3' => $this->p3,
        ]);

        return $dataProvider;
    }
}
