<?php

namespace app\controllers;

use Yii;
use app\models\Reserv;
use app\models\ReservSearch;
use app\models\Appoint;
use app\models\AppointSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ReservController implements the CRUD actions for Reserv model.
 */
class ReservController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Reserv models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReservSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Reserv model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Reserv model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($bed_id,$dep_id,$period_id,$appoint_date)
    {
        $model = new Reserv();
        $appoint = new Appoint();

        function sendToLine($message){    

            $line_api = 'https://notify-api.line.me/api/notify';
            $line_token = 'aGES7AMxqzK7EjAFyBRSBmjPUNSuKj6ArKMEZUeRI0Y';
        
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://notify-api.line.me/api/notify");
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, 'message='.$message);
            // follow redirects
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: Bearer '.$line_token,
            ]);
            // receive server response ...
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
            $server_output = curl_exec ($ch);
        
            curl_close ($ch);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $appoint->dep_id = $dep_id;
            $appoint->bed_id = $bed_id;
            $appoint->period_id = $period_id;
            $appoint->appoint_date = $appoint_date;
            $appoint->reserv_id = $model->reserv_id;
            $appoint->appoint_status = 1;
            $appoint->save(false);

            $m = 'จองคิวนวด :'.$model->reserv_id.'. ผู้รับบริการ :'.$model->reserv_name.'. วันที่ :'.$appoint->appoint_date.' เวลา :'.$appoint->getPeriodName().' เบอร์โทร : '.$model->reserv_tel;
            
            sendToLine($m);

            return $this->redirect(['view', 'id' => $model->reserv_id]);

        }

        return $this->render('create', [
            'model' => $model,
            'bed_id' => $bed_id,
            'dep_id' => $dep_id,
            'period_id' => $period_id,
            'appoint_date' => $appoint_date,    
        ]);
    }

    /**
     * Updates an existing Reserv model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->reserv_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Reserv model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Reserv model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Reserv the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Reserv::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
