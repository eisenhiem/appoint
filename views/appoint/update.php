<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Appoint */

$this->title = 'เปลี่ยนแปลงรายการนัด: ' . $model->appoint_id;
$this->params['breadcrumbs'][] = ['label' => 'รายการนัด', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->appoint_id, 'url' => ['view', 'id' => $model->appoint_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="appoint-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
