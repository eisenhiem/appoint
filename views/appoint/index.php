<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AppointSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Appoints';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appoint-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Appoint', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'appoint_id',
            // 'dep_id',
            [
                'attribute'=>'dep_id',
                'label'=>'แผนก',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getDepName();
                }
            ],
            // 'bed_id',
            [
                'attribute'=>'bed_id',
                'label'=>'เตียง',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getBedName();
                }
            ],
            //'reserv_id',
            [
                'attribute'=>'reserv_id',
                'label'=>'ชื่อผู้จอง',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return Html::label($data->getReservName().'<br>'.$data->getReservTel());
                }
            ],

            // 'period_id',
            [
                'attribute'=>'period_id',
                'label'=>'เวลา',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getPeriodName();
                }
            ],
            [
                'attribute'=>'appoint_date',
                'label'=>'วันจอง',
                'content'=>function($data){
                    return $data->appoint_date;
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                'header' => 'สถานะ',
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{status}',
                'buttons'=>[
                    'status' => function($url,$model,$key){
                      return $model->appoint_status == 2 ? Html::label('ยืนยันการจอง',['class' => 'btn btn-outline-success']):Html::a('ยืนยัน',['appoint/confirm','id'=>$model->appoint_id],['class' => 'btn btn-outline-danger']);
                    }
                ]
             ],
            // 'appoint_status',
            // 'd_update',
            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'header' => 'แก้ไขการจอง',
                'template'=>'{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                      return $model->appoint_status == 2 ? '' : Html::a('แก้ไข',['appoint/update','id'=>$model->appoint_id],['class' => 'btn btn-outline-success']);
                    }
                ]
             ],

        ],
    ]); ?>


</div>
