<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AppointSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appoint-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'appoint_id') ?>

    <?= $form->field($model, 'dep_id') ?>

    <?= $form->field($model, 'bed_id') ?>

    <?= $form->field($model, 'reserv_id') ?>

    <?= $form->field($model, 'period_id') ?>

    <?php // echo $form->field($model, 'appoint_date') ?>

    <?php // echo $form->field($model, 'appoint_status') ?>

    <?php // echo $form->field($model, 'd_update') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
