<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

use app\models\Bed;
use app\models\Department;
use app\models\Period;
use app\models\Reserv;

$dep = ArrayHelper::map(Department::find()->all(), 'dep_id', 'dep_name');
$period = ArrayHelper::map(Period::find()->all(), 'period_id', 'period_name');
$bed = ArrayHelper::map(Bed::find()->where(['dep_id' => '1'])->all(), 'bed_id', 'bed_name');
$reserv = ArrayHelper::map(Reserv::find()->all(), 'reserv_id', 'reserv_name');

/* @var $this yii\web\View */
/* @var $model app\models\Appoint */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appoint-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dep_id')->dropDownList($dep) ?>

    <?= $form->field($model, 'bed_id')->dropDownList($bed) ?>

    <?= $form->field($model, 'reserv_id')->dropDownList($reserv) ?>

    <?= $form->field($model, 'period_id')->radioList($period) ?>

    <?= $form->field($model, 'appoint_date')->widget(DatePicker::ClassName(),
    [
        'name' => 'appoint_date', 
        'type' => DatePicker::TYPE_INPUT,
        // 'size' => 'lg',
        // 'options' => ['placeholder' => 'Select date ...'],
        'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'todayHighlight' => true
        ]
    ]); ?>


    <?= $form->field($model, 'appoint_status')->radioList([ 2 => 'ยืนยัน', 1 => 'จอง', 0 => 'ยกเลิก', ], ['prompt' => '']) ?>

    <?php // $form->field($model, 'd_update')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
