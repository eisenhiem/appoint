<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DentalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dentals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dental-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Dental', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'appoint_date',
            'bed_id',
            'p1',
            'p2',
            'p3',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
