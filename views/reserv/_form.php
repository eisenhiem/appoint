<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Reserv */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reserv-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'reserv_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reserv_tel')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึกการจอง', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
