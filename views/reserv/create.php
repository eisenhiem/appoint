<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Reserv */

$this->title = 'ทำรายการจอง';
$this->params['breadcrumbs'][] = ['label' => 'Reservs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reserv-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'bed_id' => $bed_id,
        'dep_id' => $dep_id,
        'period_id' => $period_id,
        'appoint_date' => $appoint_date,
    ]) ?>

</div>
