<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReservSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reservs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reserv-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Reserv', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'reserv_id',
            'reserv_name',
            'reserv_tel',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
