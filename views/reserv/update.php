<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Reserv */

$this->title = 'Update Reserv: ' . $model->reserv_id;
$this->params['breadcrumbs'][] = ['label' => 'Reservs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->reserv_id, 'url' => ['view', 'id' => $model->reserv_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reserv-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
