<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TtmSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'นัดแพทย์แผนไทย';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttm-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            //'appoint_date',
            'bed_name',
            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                'header' => '8.30-9.30 น.',
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{empty}',
                'buttons'=>[
                    'empty' => function($url,$model,$key){
                      return $model->p1 == null ? Html::a('ว่าง',['reserv/create','bed_id'=>$model->bed_id,'dep_id'=>1,'period_id'=>1,'appoint_date'=>$model->appoint_date],['class' => 'btn btn-outline-success']):' จองแล้ว ';
                    }
                ]
             ],
             [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                'header' => '9.45-10.45 น.',
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{empty}',
                'buttons'=>[
                    'empty' => function($url,$model,$key){
                      return $model->p2 == null ? Html::a('ว่าง',['reserv/create','bed_id'=>$model->bed_id,'dep_id'=>1,'period_id'=>2,'appoint_date'=>$model->appoint_date],['class' => 'btn btn-outline-success']):' จองแล้ว ';
                    }
                ]
             ],
             [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                'header' => '11.00-12.00 น.',
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{empty}',
                'buttons'=>[
                    'empty' => function($url,$model,$key){
                      return $model->p3 == null ? Html::a('ว่าง',['reserv/create','bed_id'=>$model->bed_id,'dep_id'=>1,'period_id'=>3,'appoint_date'=>$model->appoint_date],['class' => 'btn btn-outline-success']):' จองแล้ว ';
                    }
                ]
             ],
             [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                'header' => '13.00-14.00 น.',
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{empty}',
                'buttons'=>[
                    'empty' => function($url,$model,$key){
                      return $model->p4 == null ? Html::a('ว่าง',['reserv/create','bed_id'=>$model->bed_id,'dep_id'=>1,'period_id'=>4,'appoint_date'=>$model->appoint_date],['class' => 'btn btn-outline-success']):' จองแล้ว ';
                    }
                ]
             ],
             [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                'header' => '14.15-15.15 น.',
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{empty}',
                'buttons'=>[
                    'empty' => function($url,$model,$key){
                      return $model->p5 == null ? Html::a('ว่าง',['reserv/create','bed_id'=>$model->bed_id,'dep_id'=>1,'period_id'=>5,'appoint_date'=>$model->appoint_date],['class' => 'btn btn-outline-success']):' จองแล้ว ';
                    }
                ]
             ]

        ],
    ]); ?>


</div>
