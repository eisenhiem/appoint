<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\TtmSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ttm-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class='row'>
        <div class='col-4'>
        <?= $form->field($model, 'appoint_date')->widget(DatePicker::ClassName(),
    [
        'name' => 'appoint_date', 
        'type' => DatePicker::TYPE_INLINE,
        // 'size' => 'lg',
        // 'options' => ['placeholder' => 'Select date ...'],
        'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'todayHighlight' => true
        ]
    ]); ?>
        
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
