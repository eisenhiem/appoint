<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="body-content">

        <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <div class="card">
            <?= Html::a("<img class='card-img-top' src='../web/imgs/ttm.png' alt='Card image cap'><br>".'จองคิวรับบริการแพทย์แผนไทย',
                ['ttm/index'], ['class' => 'btn btn-lg btn-outline-primary','style' => 'border-radius: 20px']) 
            ?>
            </div>
        </div>
        
        <!--
        <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <div class="card">
                <?= Html::a("<img class='card-img-top' src='../web/imgs/dental.png' alt='Card image cap'><br>".'จองคิวทำฟัน',
                    ['dental/index'], ['class' => 'btn btn-lg btn-outline-primary','style' => 'border-radius: 20px']) 
                ?>
            </div>
        </div>
        !-->
        </div>

    </div>
</div>
